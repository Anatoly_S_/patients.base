<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Research extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('research_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger("research_type_id")->unsigned();
            $table->timestamps();

        });

        Schema::create('research', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date')->nullable();;
            $table->longText('description')->nullable();;
            $table->bigInteger('patient_id')->unsigned();
            $table->timestamps();
            $table->bigInteger('research_list_id')->unsigned();
        });

        Schema::create('research_param', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type');
            $table->integer("sort")->default(500);
            $table->boolean("multiple")->default(false);
            $table->bigInteger("research_list_id")->unsigned();
            $table->timestamps();
        });


        Schema::create('research_param_value', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("research_id")->unsigned();
            $table->bigInteger("research_param_id")->unsigned();
            $table->string('value_string')->nullable();
            $table->string('value_integer')->nullable();
            $table->double('value_double')->nullable();
            $table->boolean('value_boolean')->nullable();
            $table->bigInteger('value_select')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::create('research_param_select_type_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("research_param_id")->unsigned();
            $table->string('name');
            $table->timestamps();
        });
        $idA = \Illuminate\Support\Facades\DB::table("research_type")->insertGetId(["name" => "Анализ"]);
        $idD = \Illuminate\Support\Facades\DB::table("research_type")->insertGetId(["name" => "Диагностика"]);

        $idKAK = \Illuminate\Support\Facades\DB::table("research_list")->insertGetId(["name" => "Клинический анализ крови", "research_type_id" => $idA]);
        $idG = \Illuminate\Support\Facades\DB::table("research_list")->insertGetId(["name" => "Гемоглобин", "research_type_id" => $idA]);

        $idParam1 = \Illuminate\Support\Facades\DB::table("research_param")->insertGetId(["name" => "WBC, 10^9 кл/л", 'type' => "double", "research_list_id" => $idKAK]);
        $idParam2 = \Illuminate\Support\Facades\DB::table("research_param")->insertGetId(["name" => "RBC, 10^12 кл/л", 'type' => "double", "research_list_id" => $idKAK]);
        $idParam3 = \Illuminate\Support\Facades\DB::table("research_param")->insertGetId(["name" => "HBG, г/л", 'type' => "double", "research_list_id" => $idKAK]);
        $idGParam1 = \Illuminate\Support\Facades\DB::table("research_param")->insertGetId(["name" => "Количество, г/л", 'type' => "double", "research_list_id" => $idG]);
        $idGParam2 = \Illuminate\Support\Facades\DB::table("research_param")->insertGetId(["name" => "select", 'type' => "select", "research_list_id" => $idG]);

        $idS1=\Illuminate\Support\Facades\DB::table("research_param_select_type_list")->insertGetId(["name" => "да",  "research_param_id" => $idGParam2]);
        $idS2=\Illuminate\Support\Facades\DB::table("research_param_select_type_list")->insertGetId(["name" => "нет",  "research_param_id" => $idGParam2]);

        $idResearch = \Illuminate\Support\Facades\DB::table("research")->insertGetId(["patient_id" => 1, "research_list_id" => $idKAK, "description" => "hallo"]);
        $idResearchG = \Illuminate\Support\Facades\DB::table("research")->insertGetId(["patient_id" => 1, "research_list_id" => $idG, "description" => "hallo"]);

        \Illuminate\Support\Facades\DB::table("research_param_value")->insertGetId(["research_param_id" => $idParam1, "value_double" => 1.8, "research_id" => $idResearch]);
        \Illuminate\Support\Facades\DB::table("research_param_value")->insertGetId(["research_param_id" => $idParam2, "value_double" => 1.6, "research_id" => $idResearch]);
        \Illuminate\Support\Facades\DB::table("research_param_value")->insertGetId(["research_param_id" => $idParam3, "value_double" => 1.4, "research_id" => $idResearch]);

        \Illuminate\Support\Facades\DB::table("research_param_value")->insertGetId(["research_param_id" => $idGParam1, "value_double" => 1.4, "research_id" => $idResearchG]);
        \Illuminate\Support\Facades\DB::table("research_param_value")->insertGetId(["research_param_id" => $idGParam2, "value_select" => 1, "research_id" => $idResearchG]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists("research_type");
        Schema::dropIfExists("research_list");
        Schema::dropIfExists("research");
        Schema::dropIfExists("research_param");
        Schema::dropIfExists("research_param_value");
        Schema::dropIfExists("research_param_select_type_list");
    }
}
