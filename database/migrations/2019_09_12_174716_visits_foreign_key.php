<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VisitsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("visits",function (Blueprint $table){
            $table->bigInteger("patient_id")->unsigned()->change();
            $table->foreign("patient_id",'visits_patient_id_foreign')->references("id")->on("patients")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("visits",function (Blueprint $table){
            $table->dropForeign("visits_patient_id_foreign");
            $table->dropIndex('visits_patient_id_foreign');
        });
    }
}
