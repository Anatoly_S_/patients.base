@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
    <form method="post">
        <input type="hidden" name="id" value="{{ $patient->id }}">
        {{ csrf_field() }}
        <table class="table">

            <td colspan="2">
                <a type="submit" href="{{route("all-patients")}}" class="float-left btn btn-light"> К списку</a>
            </td>

            @foreach(\App\Patient::getDescriptionFields() as $rowCode=>$rowName)
                <tr>
                    <th><label for="data-{{$rowCode}}">{{ $rowName["name"] }}</label></th>
                    <td>
                        @if($rowName["type"]=="date")
                            <input id="data-{{$rowCode}}" type="date" class="form-control" name="{{$rowCode }}"
                                   value="{{ $patient->$rowCode}}">
                        @elseif($rowName["type"]=="string")
                            <input id="data-{{$rowCode}}" type="text" class="form-control date-picker"
                                   name="{{$rowCode }}" value="{{ $patient->$rowCode}}">
                        @elseif($rowName["type"]=="checkbox")
                            <input id="data-{{$rowCode}}" type="checkbox" class="form-control" name="{{$rowCode }}"
                                   value="Y" @if($patient->$rowCode==1) checked="checked" @endif>

                        @endif

                    </td>
                </tr>
            @endforeach
            <tr>

                <td colspan="2">
                    @if($patient->id )
                        <a type="submit" onclick="return confirm('Are you sure delete?')"
                           href="{{$patient->getLinkDelete()}}" class="float-left btn btn-danger"> Удалить</a>
                    @endif
                    <input type="submit" class="float-right btn btn-primary" name="submit" value="Сохранить">
                </td>
            </tr>

        </table>
    </form>
    <table class="table">
        @if($patient->id)
            <tr>
                <td colspan="2" class="h2">Визиты</td>
            </tr>
            <tr>
                <td colspan="2"><a class="float-right btn btn-primary"
                                   href="{{route('show-visit',['id'=>0,'patient_id'=>$patient->id])}}">Создать</a>
                </td>

            </tr>
            @if($patient->visits()->count())
                <tr>
                    <td>id</td>
                    <td>Дата</td>
                </tr>

                @foreach($patient->visits()->get() as $visit)
                    <tr>
                        <td><a href="{{$visit->getLinkModify($patient->id)}}">{{ $visit->id }}</a></td>
                        <td>{{$visit->date}}</td>
                    </tr>
                @endforeach
            @endif
        @endif
    </table>



    <table class="table">
        @if($patient->id)
            <tr>
                <td colspan="3" class="h2">Исследования</td>
            </tr>
            <tr>
                <td colspan="3">
                    <form>

                        <a class="float-right btn btn-primary" onclick="
                            var selectValue= document.getElementById('researchListValue').value;
                            this.setAttribute('href',this.getAttribute('href').replace('#research_list_id#',selectValue));

"
                           href="{{route('research-update',['patient_id'=>$patient->id,"research_list_id"=>"#research_list_id#","research_id"=>0])}}">Создать</a>

                        <select name="" class="float-right btn" id="researchListValue">
                            @foreach($researchList as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>


                    </form>
                </td>

            </tr>
            @if($patient->researches()->count())
                <tr>
                    <td>id</td>
                    <td>Имя</td>
                    <td>Дата</td>
                </tr>

                @foreach($patient->researches()->get() as $research)
                    <tr>
                        <td><a href="{{$research->getLinkModify($patient->id)}}">{{ $research->id }}</a></td>
                        <td>{{$research->listEntity()->first()->name}}</td>
                        <td>{{$research->date}}</td>
                    </tr>
                @endforeach
            @endif
        @endif
    </table>



    <script>

    </script>


@endsection
