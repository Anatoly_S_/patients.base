@extends('layouts.app')
@section('title', "")
@section('content')


    <table class="table">
        <thead>
        <tr>

            <th colspan="5"><a class="float-right btn btn-primary" href="{{route('update-patient',['id'=>0])}}">Создать</a></th>
        </tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Отчество</th>
            <th scope="col">Дата рождения</th>
        </tr>
        </thead>
        <tbody>


        @foreach ($patients as $patient)
            <tr>
                <th scope="row"><a href="{{$patient->getLinkModify()}}">{{ $patient->id }}</a></th>
                <td>{{ $patient->name }}</td>
                <td>{{ $patient->surname }}</td>
                <td>{{ $patient->secondname }}</td>
                <td>{{ $patient->birthdate }}</td>
            </tr>
        @endforeach


        </tbody>
    </table>
    {{ $patients->onEachSide($perPage)->links() }}



@endsection
