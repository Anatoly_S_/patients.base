@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
    <form method="post">
        <input type="hidden" name="id" value="{{ $research->id }}">
        {{ csrf_field() }}
        <table class="table">
            <tr>
                <td colspan="2">
                    <a type="submit" href="{{route("show-patient",["id"=>$patient_id])}}"
                       class="float-left btn btn-light"> К пациенту</a>
                </td>
            </tr>

            @foreach($arResult as $arParamItem)

                <tr>

                    <th><label for="data-{{$arParamItem['id']}}">{{ $arParamItem['name'] }}</label></th>
                    <td>
                        @foreach($arParamItem["value"] as $arKey=>$arValue)

                            @if($arParamItem["type"]=="select")
                                <select  class="form-control" name="param[{{$arParamItem['id']}}][{{$arKey}}]">
                                    @foreach($arParamItem['select'] as $id=>$name)
                                        <option  @if($id==$arValue) selected="" @endif value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            @else
                                <input id="data-{{$arKey}}" type="text" class="form-control"
                                       name="param[{{$arParamItem['id']}}][{{$arKey}}]"
                                       value="{{ $arValue}}">
                            @endif
                        @endforeach


                    </td>
                </tr>
            @endforeach
<tr>
    <td colspan="2"><input type="submit" class="float-right btn btn-primary" name="submit" value="Сохранить"></td>
</tr>
        </table>
    </form>
    {{ debug($arResult) }}
@endsection
