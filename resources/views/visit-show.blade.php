@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
    <form method="post">
        <input type="hidden" name="id" value="{{ $visit->id }}">
        {{ csrf_field() }}
        <table class="table">

            <td colspan="2">
                <a type="submit" href="{{route("show-patient",['id'=>$patient_id])}}" class="float-left btn btn-light">
                    К списку</a>
            </td>

            @foreach(\App\Visit::getDescriptionFields() as $rowCode=>$rowName)
                <tr>
                    <th><label for="data-{{$rowCode}}">{{ $rowName["name"] }}</label></th>
                    <td>
                        @if($rowName["type"]=="date")
                            <input id="data-{{$rowCode}}" type="date" class="form-control" name="{{$rowCode }}"
                                   value="{{ $visit->$rowCode}}">
                        @elseif($rowName["type"]=="datetime")
                            <input id="data-{{$rowCode}}" type="datetime" class="form-control" name="{{$rowCode }}"
                                   value="{{ $visit->$rowCode}}">
                        @elseif($rowName["type"]=="checkbox")
                            <input id="data-{{$rowCode}}" type="checkbox" class="form-control" name="{{$rowCode }}"
                                   value="Y" @if($visit->$rowCode==1) checked="checked" @endif>
                        @elseif($rowName["type"]=="html")
                            <textarea id="data-{{$rowCode}}" type="text" class="form-control date-picker"
                                      name="{{$rowCode }}">{{htmlspecialchars($visit->$rowCode,ENT_QUOTES)}}</textarea>
                        @else
                            <input id="data-{{$rowCode}}" type="text" class="form-control date-picker"
                                   name="{{$rowCode }}" value="{{ $visit->$rowCode}}">
                        @endif

                    </td>
                </tr>
            @endforeach
            <tr>

                <td colspan="2">
                    <input type="submit" class="float-right btn btn-primary" name="submit" value="Сохранить">
                </td>
            </tr>

        </table>


    </form>




@endsection
