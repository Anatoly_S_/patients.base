<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PatientController@all')->name("all-patients");
Route::post('/patient/{id}/update/', 'PatientController@update')->name("update-patient");
Route::get('/patient/{id}/delete/', 'PatientController@delete')->name("delete-patient");
Route::get('/patient/{id}/update/', 'PatientController@show')->name("show-patient");
//Визиты
Route::get('/patient/{patient_id}/visit/{id}/update/', 'VisitController@show')->name("show-visit");
Route::post('/patient/{patient_id}/visit/{id}/update/', 'VisitController@update')->name("update-visit");
Route::get('/patient/{patient_id}/visit/{id}/delete/', 'VisitController@delete')->name("delete-visit");




Route::get('/patient/{patient_id}/researches/{research_list_id}/{research_id}/update/', 'ResearchController@show')->name("research-show");

Route::post('/patient/{patient_id}/researches/{research_list_id}/{research_id}/update/', 'ResearchController@update')->name("research-update");


