<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchParamValue extends Model
{
    protected $table="research_param_value";
    protected $primaryKey='id';
    public $incrementing=true;


    public function param(){
        return $this->hasOne('App\ResearchParam',"id","research_param_id");
    }

    public function selectValue(){
        return $this->hasOne('App\ResearchParamSelectTypeList',"id","value_select");
    }


}
