<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchParam extends Model
{
    protected $table="research_param";
    protected $primaryKey='id';
    public $incrementing=true;

    public $arTypes=[
        "string","integer","double","boolean","select"
    ];

    public function listEntity(){
        return $this->hasOne('App\ResearchList',"id","research_list_id");
    }

    public function selectValue(){
        return $this->hasMany('App\ResearchParamSelectTypeList',"research_param_id","id");
    }

}
