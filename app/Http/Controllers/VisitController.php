<?php

namespace App\Http\Controllers;

use App\Visit;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
class VisitController extends Controller
{


    public function delete(Request $request,$patient_id, $id)
    {

        if (intval($id)) {
            $visit = Visit::find(intval($id));
            if (!$visit||$visit->patient_id!=$patient_id) {
                throw new NotFoundHttpException();
            }

        } else {
            throw new NotFoundHttpException();
        }

        $visit->delete();
        return redirect()->route("show-patient",$patient_id);
    }


    public function update(Request $request,$patient_id, $id)
    {


        if (intval($id)) {
            $visit = Visit::find(intval($id));
            if (!$visit||$visit->patient_id!=$patient_id) {
                throw new NotFoundHttpException();
            }

        } elseif ($id === "0") {
            $visit = new Visit();
            $visit->patient_id=$patient_id;

        } else {
            throw new NotFoundHttpException();
        }


        $visit->date = $request->date;
        $visit->description = $request->description;
        $visit->save();

        return redirect()->route("show-visit", ['id' => $visit->id,"patient_id"=>$visit->patient_id]);

    }

    public function show(Request $request, $patient_id,$id)
    {

        if (intval($id)) {
            $_visit = Visit::find(intval($id));

            if ($_visit&& $_visit->patient_id==$patient_id) {
                $visit = $_visit;
            } else {
                throw new NotFoundHttpException();
            }


        } elseif ($id === "0") {


            $visit = new Visit();

        } else {
            throw new NotFoundHttpException();
        }

        return view('visit-show', ['visit' => $visit,"patient_id"=>$visit->patient_id]);

    }
}
