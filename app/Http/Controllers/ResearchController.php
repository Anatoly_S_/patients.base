<?php

namespace App\Http\Controllers;

use App\Research;
use App\ResearchList;
use App\ResearchParamValue;
use App\ResearchType;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ResearchController extends Controller
{

    public function update(Request $request, $patientId, $researchListEntityId, $researchId)
    {
        $research = ResearchList::find($researchListEntityId);

        if (!$researchId) {
            $research = new Research();
            $research->research_list_id = $researchListEntityId;
            $research->patient_id = $patientId;
            $research->save();
        } else {
            $research = Research::find($researchId);
            if (!$research || $research->research_list_id != $researchListEntityId) {
                throw new NotFoundHttpException();
            }
        }


        $researchParamsCollection = $research->params()->orderBy("sort")->get();

        $researchValues = $research->values()->get();

        //обходим параметры
        $arParamsRequest = $request->get('param');

        foreach ($researchParamsCollection as $paramItem) {
            $valuePropertyName = "value_" . $paramItem->type;
            //значения текущего параметра
            $valuesCollection = $researchValues->where("research_param_id", "=", $paramItem->id);
            //var_dump("<pre>",get_class($valuesCollection),$valuesCollection->diffKeysUsing()->toArray());die();
            foreach ($arParamsRequest[$paramItem->id] as $paramId=>$paramValue__fromRequest) {
                $_paramValueCollection=$valuesCollection->where("id","=",$paramId);
                if($_paramValueCollection->count()){
                    $_paramValue=$_paramValueCollection->first();

                }else{
                    $_paramValue=new ResearchParamValue();
                    $_paramValue->research_param_id=$paramItem->id;
                    $_paramValue->research_id=$research->id;
                }
                $_paramValue->$valuePropertyName=$paramValue__fromRequest;
                $_paramValue->save();
            }


        }
      //  var_dump($arParamsRequest);
       // die();

        return redirect()->route('research-update', ["patient_id" => $patientId, "research_list_id" => $researchListEntityId, "research_id" => $research->id]);
    }


    public function show(Request $request, $patientId, $researchListEntityId, $researchId)
    {
        $researchList = ResearchList::find($researchListEntityId);

        if (!$researchList) {
            throw new NotFoundHttpException();
        }
        if (!$researchId) {
            $research = new Research();
            $research->research_list_id = $researchListEntityId;
        } else {
            $research = Research::find($researchId);
            if (!$research || $research->research_list_id != $researchListEntityId) {
                throw new NotFoundHttpException();
            }
        }


        $researchParamsCollection = $research->params()->orderBy("sort")->get();

        $researchValues = $research->values()->get();
        $arResearchesParams = [];
        foreach ($researchParamsCollection as $paramItem) {
            $valuesCollection = $researchValues->where("research_param_id", "=", $paramItem->id);


            $arResearchesParams[$paramItem->id] = \App\Helpers\Research::retrieveValue($valuesCollection, $paramItem);
        }

//var_dump("<pre>",$arResearchesParams);
        //debug($arResearchesParams);

        return view('research-update-form', ['arResult' => $arResearchesParams, "research" => $research, "patient_id" => $patientId]);
    }


}
