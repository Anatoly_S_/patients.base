<?php

namespace App\Http\Controllers;

use App\ResearchList;
use Illuminate\Support\Facades\DB;
use App\Patient;
use Illuminate\Http\Request;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PatientController extends Controller
{
    public function getPaginationRecordCount()
    {
        return 25;
    }

    public function delete(Request $request, $id)
    {

        if (intval($id)) {
            $patient = Patient::find(intval($id));
            if (!$patient) {
                throw new NotFoundHttpException();
            }

        } else {
            throw new NotFoundHttpException();
        }

        $patient->delete();
        return redirect()->route("all-patients");
    }

    public function all()
    {

        return view('patients', ['patients' => Patient::paginate($this->getPaginationRecordCount()), "perPage" => $this->getPaginationRecordCount()]);
    }

    public function update(Request $request, $id)
    {


        if (intval($id)) {
            $patient = Patient::find(intval($id));
            if (!$patient) {
                throw new NotFoundHttpException();
            }

        } elseif ($id === "0") {
            $patient = new Patient();

        } else {
            throw new NotFoundHttpException();
        }


        $patient->name = $request->name;
        $patient->surname = $request->surname;
        $patient->secondname = $request->secondname;
        $patient->birthdate = $request->birthdate;
        $patient->save();

        return redirect()->route("show-patient", ['id' => $patient->id]);

    }

    public function show(Request $request, $id)
    {

        if (intval($id)) {
            $_patient = Patient::find(intval($id));

            if ($_patient) {
                $patient = $_patient;
            } else {
                throw new NotFoundHttpException();
            }


        } elseif ($id === "0") {


            $patient = new Patient();

        } else {
            throw new NotFoundHttpException();
        }

       // var_dump($patient->researches()->get()->first()->listEntity()->get()->first()->name);
       // die();
        return view('patient-show', ['patient' => $patient,'researchList'=>ResearchList::all()]);

    }


}
