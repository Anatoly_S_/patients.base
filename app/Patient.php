<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table="patients";
    protected $primaryKey='id';
    public $incrementing=true;

    public function getLinkModify(){
        return route("show-patient",["id"=>$this->id]);
    }

    public function getLinkDelete(){
        return route("delete-patient",["id"=>$this->id]);
    }

    public function visits()
    {
        return $this->hasMany('App\Visit',"patient_id","id");
    }

    public function researches()
    {
        return $this->hasMany('App\Research',"patient_id","id");
    }

    /**
     * @return array
     */
public static  function getDescriptionFields(){
    return [
        "name"=>[
            "name"=>"Имя",
            "type"=>"string",
        ],
        "surname"=>[
            "name"=>"Фамилия",
            "type"=>"string",
        ],
        "secondname"=>[
            "name"=>"Отчество",
            "type"=>"string",
        ],
        "birthdate"=>[
            "name"=>"Дата рождения",
            "type"=>"date",
        ],

    ];
}

}
