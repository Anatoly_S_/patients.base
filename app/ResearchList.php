<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchList extends Model
{
    protected $table="research_list";
    protected $primaryKey='id';
    public $incrementing=true;

    public function type(){
        return $this->hasOne('App\ResearchType',"id","reasearch_type_id");
    }

    public function params(){
        return $this->hasMany('App\ResearchParams',"research_id","id");
    }
}
