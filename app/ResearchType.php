<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchType extends Model
{
    protected $table="research_type";
    protected $primaryKey='id';
    public $incrementing=true;

    public function listsEntities(){
        return $this->hasMany('App\ResearchParam',"research_type_id","id");
    }
}
