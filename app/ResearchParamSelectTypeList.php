<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchParamSelectTypeList extends Model
{
    protected $table="research_param_select_type_list";
    protected $primaryKey='id';
    public $incrementing=true;


    public function param(){
        return $this->hasOne('App\ResearchParam',"id","research_param_id");
    }
}
