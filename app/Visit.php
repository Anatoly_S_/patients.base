<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = "visits";
    protected $primaryKey = 'id';
    public $incrementing = true;

    public function getLinkModify($patientId)
    {
        return route("show-visit", ["id" => $this->id, "patient_id" => $patientId]);
    }

    public function patient()
    {
        return $this->hasOne('App\Patient', "id", "patient_id");
    }


    public static function getDescriptionFields()
    {
        return [

            "date"=>[
                "name"=>"Дата",
                "type"=>"datetime",
            ],

            "description"=>[
                "name"=>"Описание",
                "type"=>"html",
            ],
        ];
    }

}
