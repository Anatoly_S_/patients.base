<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    protected $table="research";
    protected $primaryKey='id';
    public $incrementing=true;

    public function getLinkModify($patientId)
    {
        return route("research-show", ["research_id" => $this->id, "patient_id" => $patientId,"research_list_id"=>$this->research_list_id]);
    }

    public function patient(){
        return $this->hasOne('App\Patient',"id","patient_id");
    }

    public function listEntity(){
        return $this->hasOne('App\ResearchList',"id","research_list_id");
    }
    public function params(){
        return $this->hasManyThrough('App\ResearchParam','App\ResearchList',"id","research_list_id","research_list_id");
    }

    public function values(){
        return $this->hasMany('App\ResearchParamValue',"research_id","id");
    }
}

