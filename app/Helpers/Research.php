<?php

namespace App\Helpers;

use App\ResearchParamSelectTypeList;

class Research
{
    static function retrieveValue($paramValue, $param)
    {
        $arValues= $param->toArray();
        $valuePropertyName = "value_" . $param->type;

        if ($paramValue->count()) {
            foreach ($paramValue as $item) {
                $arValues["value"][$item->id] = $item->$valuePropertyName;
            }
        } else {
            $arValues["value"]['n0'] = "";
        }


        if ($param->type=="select") {
           $arValues["select"]=array_column($param->selectValue()->get()->toArray(),"name","id");
        }
        return $arValues;
    }

}